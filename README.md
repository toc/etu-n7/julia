# Julia course

<img src="https://gitlab.irit.fr/toc/etu-n7/julia/-/raw/main/assets/TSE_Logo_2019.png" alt="TSE" height="200" style="margin-left:50px"/>

This repository contains Julia courses to M1 and M2 at TSE. 

## M1 course

The course has moved to [https://ocots.github.io/course-tse-julia](https://ocots.github.io/course-tse-julia).